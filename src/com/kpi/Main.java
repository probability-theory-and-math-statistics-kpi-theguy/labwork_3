package com.kpi;

import java.io.*;
import java.util.Scanner;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
        final double MATHEXP = 0.42441;
        final double SIGMA = 0.2646;
        final double COV = -0.02097;
        final double CORRELATION = -0.29951;
        Scanner scan = new Scanner(in);
        out.println("We have a system of two random variables: X and Y. Enter the number of sample:");
        int n = scan.nextInt();
        double[] arrayX = new double[n];
        double[] arrayY = new double[n];
        Generation gen = new Generation();
        for(int i = 0; i < n; i++){
            arrayX[i] = gen.GenerateXi(gen.GenerateRi());
        }
        for(int i = 0; i < n; i++){
            arrayY[i] = gen.GenerateYi(arrayX[i], gen.GenerateRi());
        }
        out.println("Our arrays with size of "+n+" is saved in file: С://generator2.txt");
        File fil = new File("A://generator2.txt");
        try {
            if(!fil.exists()){
                fil.createNewFile();
            }
            BufferedWriter buff = new BufferedWriter(new FileWriter(fil));
            for(int i = 0; i < arrayX.length; i++){
                buff.write(arrayX[i] + " ");
            }
            buff.write("         Y: ");
            for(int i = 0; i < arrayY.length; i++){
                buff.write(arrayY[i] + " ");
            }
            buff.flush();
            buff.close();
        }catch(IOException e){
            err.println(e.getMessage());
        }
        out.println("Now, let's check is a distribution is correct");
        Characteristics chart = new Characteristics(arrayX, arrayY);
        out.println("Experimental Math expectation of X: "+chart.mathExpX()+" ; Theoretical: "+MATHEXP);
        out.println("Experimental Math expectation of Y: "+chart.mathExpY()+" ; Theoretical: "+MATHEXP);
        out.println("Experimental Standard deviation of X: "+chart.dispersionX()+" ; Theoretical: "+SIGMA);
        out.println("Experimental Standard deviation of Y: "+chart.dispersionY()+" ; Theoretical: "+SIGMA);
        out.println("Experimental Covariance of X: "+chart.covariance()+" ; Theoretical: "+COV);
        out.println("Experimental Correlation of X: "+chart.correlation()+" ; Theoretical: "+CORRELATION);
    }
}
