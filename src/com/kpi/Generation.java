package com.kpi;

import java.util.Random;

/**
 * Class of Generation of two random variables: X and Y
 * @author Yuriy
 * @version 1.0
 */
public class Generation {

    /**
     * Method, that gives a random number in (0,1)
     * @return
     * A number, that comes from bult-in generator
     */
    public double GenerateRi(){
        Random r = new Random();
        double rand = r.nextDouble();
        return rand;
    }

    /**
     * Method, that returns an integrated function f(x), and also there taken into account the actual value of the area that is cut off
     * @param x
     * Parameter of argument of x, of which the function will show the value of it
     * @param r
     * A random number, generated from bult-in generator, that show an area of fragment
     * @return
     * Value of this function, in this values of x and r
     */
    public double FuncX(double x, double r){
        return ((2*(Math.asin(x)+x*Math.sqrt(1-Math.pow(x, 2)))/Math.PI) - r);
    }

    /**
     * Method, where Xi number generates. We use a bisection method with some accuracy because the exact calculation with this integrated function doesn't exist
     * @param r
     * A random number from bult-in generator
     * @return
     * Number X of this distribution
     */
    public double GenerateXi(double r){
        double a = 0;
        double b = 1;
        double e = 0.0001;
        double x = a;
        double xt;
        do{
            xt = x;
            x = (a + b)/2;
            if(FuncX(x, r)*FuncX(a, r) < 0){
                b = x;
            }
            else if(FuncX(x, r) * FuncX(b, r) < 0){
                a = x;
            }
        }while (Math.abs(xt - x) >= e);
        return x;
    }

    /**
     * Method, where Yi generates using a number Xi. In this helps a function of a condtitional distribution
     * @param x
     * Number of Xi, that is an alternative for Yi number
     * @param r
     * A random number from bult-in generator
     * @return
     * Number Y of this distribution
     */
    public double GenerateYi(double x, double r){
        return (r*Math.sqrt(1 - Math.pow(x, 2)));
    }
}
