package com.kpi;

/**
 * Class of Characteristics of this distribution
 * @author Yuriy
 * @version 1.0
 */
public class Characteristics {
    private double mathX;
    private double mathY;
    private double sigmaX;
    private double sigmaY;
    private double COV;
    private double[] testArrayX;
    private double[] testArrayY;

    //Setters
    public void setTestArrayX(double[] testArrayX) {
        this.testArrayX = testArrayX;
    }

    public void setTestArrayY(double[] testArrayY) {
        this.testArrayY = testArrayY;
    }

    /**
     * Constructor of this class, where we take arrays of Xi and Yi
     * @param tstX
     * Array of Xi, which transfers into a field of this class
     * @param tsty
     * Array of Yi, which transfers into a field of this class
     */
    public Characteristics(double[] tstX, double[] tsty){
        setTestArrayX(tstX);
        setTestArrayY(tsty);
    }

    /**
     * Method of experimental calculation of expectation that consists of calculation the arithmetic mean of the arrayX
     * @return
     * Obtaining the number, which passes to calculate the dispersionX and correlation
     */
    public double mathExpX(){
        double sum = 0;
        for (int i = 0; i < testArrayX.length; i++){
            sum += testArrayX[i];
        }
        mathX = sum/testArrayX.length;
        return mathX;
    }

    /**
     * Method of experimental calculation of expectation that consists of calculation the arithmetic mean of the arrayY
     * @return
     * Obtaining the number, which passes to calculate the dispersionY and correlation
     */
    public double mathExpY(){
        double sum = 0;
        for (int i = 0; i < testArrayY.length; i++){
            sum += testArrayY[i];
        }
        mathY = sum/testArrayY.length;
        return mathY;
    }

    /**
     * Method that calculates experimental values of dispersion and standard deviation of arrayX
     * @return
     * Obtaining the number that expresses the experimental value of deviation which passes to calculate the correlation
     */
    public double dispersionX(){
        double sum = 0;
        for(int i = 0; i < testArrayX.length; i++){
            sum += Math.pow(testArrayX[i] - mathX, 2);
        }
        sigmaX = Math.sqrt(sum/testArrayX.length);
        return sigmaX;
    }

    /**
     * Method that calculates experimental values of dispersion and standard deviation of arrayY
     * @return
     * Obtaining the number that expresses the experimental value of deviation which passes to calculate the correlation
     */
    public double dispersionY(){
        double sum = 0;
        for(int i = 0; i < testArrayY.length; i++){
            sum += Math.pow(testArrayY[i] - mathY, 2);
        }
        sigmaY = Math.sqrt(sum/testArrayY.length);
        return sigmaY;
    }

    /**
     * Method of calculation of covariance of both arrays of X and Y
     * @return
     * Obtains a number of covariance, which passes to calculate a correlation
     */
    public double covariance(){
        double sum = 0;
        for(int i = 0; i < testArrayX.length; i++){
            sum += ((testArrayX[i] - mathX) * (testArrayY[i] - mathY));
        }
        COV = sum/testArrayX.length;
        return COV;
    }

    /**
     * Method of calculation of correlation of the whole distribution
     * @return
     * Obtains a number of correlation which is a experimental
     */
    public double correlation(){
        return (COV / (sigmaX * sigmaY));
    }
}
